﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using DJJServer.Models.Spotify;

namespace DJJServer.Helpers
{
    public class HttpClientHelper
    {
        public static T Get<T> (string url)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                var jsonString = client.GetAsync(url).Result.Content.ReadAsStringAsync().Result.ToString();
                var model = JsonConvert.DeserializeObject<T>(jsonString);
                return model;
            }
        }
    }
}
