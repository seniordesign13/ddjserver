﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DJJServer.Models.Api;

namespace DJJServer.Data
{
    public class Session
    {
        public string Id { get; set; }
        public List<User> Users { get; set; }
        public Queue<string> RecentSongs { get; set; }
        public DateTime LastUpdate { get; set; }

        public void Touch()
        {
            this.LastUpdate = DateTime.Now;
        }
    }
}
