﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using DJJServer.Models;
using DJJServer.Models.Api.REST;
using DJJServer.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DJJServer.Controllers
{
    [Route("api/v1/[action]")]
    public class DigitalDjController : Controller
    {
        private readonly ISessionService _sessionService;
        private readonly ISpotifyTokenSwapService _spotifyTokenSwapService;

        private readonly PlaylistGenerator _playlistGenerator;

        public DigitalDjController(ISessionService sessionService, ISpotifyTokenSwapService spotifyTokenService)
        {
            _sessionService = sessionService;
            _playlistGenerator = new PlaylistGenerator(sessionService);
            _spotifyTokenSwapService = spotifyTokenService;
        }

        [HttpGet]
        public JsonResult GetPlaylist([FromQuery] string sessionId, [FromQuery] int playlistLength, [FromQuery] int songsToRecycle = 0)
        {
            var playlist = _playlistGenerator.GeneratePlaylist(sessionId,
                    playlistLength,
                    songsToRecycle
                );

            return Json(playlist);
        }

        [HttpPost]
        public JsonResult NewSession()
        {
            var sessionId = _sessionService.NewSession();
            return Json(sessionId);
        }

        [HttpPost]
        public JsonResult EndSession([FromBody] EndSessionRequest endSessionRequest)
        {
            return Json(_sessionService.EndSession(endSessionRequest.SessionId));
        }

        [HttpPost]
        public JsonResult NewUser([FromBody] NewUserRequest newUserRequest)
        {
            return Json(_sessionService.NewUser(newUserRequest.SessionId, newUserRequest.SongIds));
        }

        [HttpPost]
        public JsonResult RemoveUser([FromBody] RemoveUserRequest removeUserRequest)
        {
            return Json(_sessionService.RemoveUser(removeUserRequest.SessionId, removeUserRequest.UserId));
        }

        [HttpPost]
        public JsonResult UpdateUser([FromBody] UpdateUserRequest updateUserRequest)
        {
            return Json(_sessionService.UpdateUser(updateUserRequest.SessionId, updateUserRequest.UserId, updateUserRequest.SongIds));
        }

        [HttpGet]
        public JsonResult Swap([FromQuery] String token)
        {
            return Json(_spotifyTokenSwapService.SwapToken(token));
        }
    }
}
