﻿namespace DJJServer.Models.Spotify
{
    public class SimplifiedArtist
    {
        public ExternalUrlObject External_urls { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Uri { get; set; }
    }
}
