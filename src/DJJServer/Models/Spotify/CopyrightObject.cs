﻿namespace DJJServer.Models.Spotify
{
    public class CopyrightObject
    {
        public string Text { get; set; }
        public string Type { get; set; }
    }
}