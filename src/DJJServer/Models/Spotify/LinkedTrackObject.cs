﻿namespace DJJServer.Models.Spotify
{
    public class LinkedTrackObject
    {
        public ExternalUrlObject External_Urls { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public string Type { get; set; }
        public string Uri { get; set; }
    }
}