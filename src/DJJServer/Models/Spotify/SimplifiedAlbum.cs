﻿using System.Collections.Generic;

namespace DJJServer.Models.Spotify
{
    public class SimplifiedAlbum
    {
        public string Album_Type { get; set; }
        public List<SimplifiedArtist> Artists { get; set; }
        public List<string> Available_Markets { get; set; }
        public ExternalUrlObject External_Urls { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public List<ImageObject> Images { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Uri { get; set; }
    }
}
