﻿using System.Collections.Generic;

namespace DJJServer.Models.Spotify
{
    public class Track
    {
        public SimplifiedAlbum Album { get; set; }
        public List<SimplifiedArtist> Artists { get; set; }
        public List<string> Available_Markets { get; set; }
        public int Disc_Number { get; set; }
        public int Duration_Ms { get; set; }
        public bool Explicit { get; set; }
        public ExternalIdObject External_Ids { get; set; }
        public ExternalUrlObject External_Urls { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public bool Is_Playable { get; set; }
        public LinkedTrackObject Linked_From { get; set; }
        public string Name { get; set; }
        public int Popularity { get; set; }
        public string Preview_Url { get; set; }
        public string track_number { get; set; }
        public string type { get; set; }
        public string Uri { get; set; }



    }
}
