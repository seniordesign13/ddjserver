﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Spotify
{
    public class Artist
    {
        public ExternalUrlObject External_urls { get; set; }
        public Followers Followers { get; set; }
        public List<string> Genres { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public int Popularity { get; set; }
        public string Type { get; set; }
        public string Uri { get; set; }
    }
}
