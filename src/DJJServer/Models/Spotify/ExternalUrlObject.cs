﻿namespace DJJServer.Models.Spotify
{
    public class ExternalUrlObject
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}