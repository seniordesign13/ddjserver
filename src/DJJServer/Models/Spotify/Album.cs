﻿using System.Collections.Generic;

namespace DJJServer.Models.Spotify
{
    public class Album
    {
        public string Album_Type { get; set; }
        public List<SimplifiedArtist> Artists { get; set; }
        public List<string> Available_Markets { get; set; }
        public List<CopyrightObject> CopyrightObjects { get; set; }
        public ExternalIdObject External_Ids { get; set; }
        public ExternalUrlObject External_Urls { get; set; }
        public List<string> Genres { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public List<ImageObject> Images { get; set; }
        public string Label { get; set; }
        public string Name { get; set; }
        public int Popularity { get; set; }
        public string Release_Date { get; set; }
        public string Release_Date_Precision { get; set; }
        public List<PagingObject<Track>> Tracks { get; set; }
        public string Type { get; set; }
        public string Uri { get; set; }
    }
}