﻿namespace DJJServer.Models.Spotify
{
    public class ImageObject
    {
        public int Height { get; set; }
        public string Url { get; set; }
        public int Width { get; set; }
    }
}