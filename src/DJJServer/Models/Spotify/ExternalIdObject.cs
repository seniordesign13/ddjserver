﻿namespace DJJServer.Models.Spotify
{
    public class ExternalIdObject
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}