﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.SessionData
{
    public class SessionData
    {
        public string session_id { get; set; }
        public string users { get; set; }
        public string recent_songs { get; set; }
        public DateTime last_update { get; set; }

        public SessionData(string sessionId, string users, string recentSongs, DateTime lastUpdate)
        {
            this.session_id = sessionId;
            this.users = users;
            this.recent_songs = recentSongs;
            this.last_update = lastUpdate;
        }
    }
}
