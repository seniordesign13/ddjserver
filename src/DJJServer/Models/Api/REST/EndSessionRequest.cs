﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Api.REST
{
    public class EndSessionRequest
    {
        public string SessionId { get; set; }

        public EndSessionRequest(string sessionId)
        {
            SessionId = sessionId;
        }
    }
}
