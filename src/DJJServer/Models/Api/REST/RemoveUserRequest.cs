﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Api.REST
{
    public class RemoveUserRequest
    {
        public string SessionId { get; set; }
        public string UserId { get; set; }

        public RemoveUserRequest(string sessionId, string userId)
        {
            SessionId = sessionId;
            UserId = userId;
        }
    }
}
