﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Api.REST
{
    public class UpdateUserRequest
    {
        public string SessionId { get; set; }
        public string UserId { get; set; }
        public List<string> SongIds { get; set; }

        public UpdateUserRequest(string sessionId, string userId, List<string> songIds)
        {
            SessionId = sessionId;
            UserId = userId;
            SongIds = songIds;
        }
    }
}
