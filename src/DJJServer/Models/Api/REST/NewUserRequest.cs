﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Api.REST
{
    public class NewUserRequest
    {
        public string SessionId { get; set; }
        public List<string> SongIds { get; set; }

        public NewUserRequest(string sessionId, List<string> songIds)
        {
            SessionId = sessionId;
            SongIds = songIds;
        }
    }
}
