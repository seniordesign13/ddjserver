﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Api
{
    public class Song
    {
        public string SongId { get; set; }
        public List<string> Artists { get; set; }
        public List<string> Genres { get; set; }
        public int Weight { get; set; }
        public string Album { get; set; }

        public Song(string songId, string album, List<string> artists, List<string> genres, int weight)
        {
            SongId = songId;
            Album = album;
            Artists = artists;
            Genres = genres;
            Weight = weight;
        }
    }
}
