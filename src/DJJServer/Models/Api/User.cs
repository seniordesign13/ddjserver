﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Models.Api
{
    public class User
    {
        public string UserId { get; set; }
        public List<string> SongIds { get; set; }

        public User(string userId, List<string> songIds)
        {
            UserId = userId;
            SongIds = songIds;
        }

        public override string ToString()
        {
            var stringRepresentation = "UserId: " + UserId + Environment.NewLine;
            stringRepresentation += "SongIds: " + Environment.NewLine;
            foreach (var songId in SongIds)
            {
                stringRepresentation += songId + ", ";
            }

            return stringRepresentation;
        }
    }
}
