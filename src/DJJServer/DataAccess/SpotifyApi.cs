﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DJJServer.DataAccess
{
    public class SpotifyApi
    {
        private static HttpClient _client;
        private static SpotifyApi _spotifyApi;

        private SpotifyApi()
        {
            _client = new HttpClient {BaseAddress = new Uri("https://api.spotify.com/")};
            // New code:
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Can I just add this to each of the methods and 
        }

        public static SpotifyApi GetSpotifyApi()
        {
            return _spotifyApi ?? (_spotifyApi = new SpotifyApi());
        }

        private string MakeWebRequest(string path)
        {
            var listOfResponse = new List<string>();
            string responseInString = null;
            HttpResponseMessage response = _client.GetAsync(path).Result;
            if (response.IsSuccessStatusCode)   
            {
                responseInString = response.Content.ReadAsStringAsync().Result;
            }
            
            return responseInString;
        }





        /*
         limit	Optional. The number of entities to return. Default: 20. Minimum: 1. Maximum: 50. For example: limit=2
         offset	Optional. The index of the first entity to return. Default: 0 (i.e., the first track). Use with limit to get the next set of entities.
         time_range	Optional. Over what time frame the affinities are computed. Valid values: long_term (calculated from several years of data and including all new data as it becomes available), medium_term (approximately last 6 months), short_term (approximately last 4 weeks). Default: medium_term. 
         */
        public List<string> GetTopArtistsForAUser(string oAuthToken)
        {
            throw new NotImplementedException();
        }

        public  string GetTopTracksForAUser(string oAuthToken)
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", oAuthToken);

            return MakeWebRequest("v1/me/top/tracks?limit=50");            
        }



        public List<string> GetCurrentSavedTracksForAUser(string oAuthToken)
        {
            throw new NotImplementedException();
        }
        private List<string> GetCurrentSavedTracksForAUser(string oAuthToken, int limit, int offset)
        {
            throw new NotImplementedException();
        }


        public List<string> GetArtistTopTracks(string oAuthToken, string spotifyArtistId, string countryCode = "US")
        {
            throw new NotImplementedException();
        }


        public List<string> GetArtistSpotifyId(string oAuthToken, string artistName)
        {
            throw new NotImplementedException();
        }



        public List<string> GetPlaylistTracks(string oAuthToken, string ownerId, string playlistId)
        {
            throw new NotImplementedException();
        }

        public List<string> GetListOfFeaturedPlaylists(string oAuthToken)
        {
            throw new NotImplementedException();

        }
    }
}
