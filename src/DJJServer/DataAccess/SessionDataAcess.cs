﻿using DJJServer.Models.SessionData;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.DataAccess
{
    public class SessionDataAccess
    {
        public string connectionString = "Data Source=digitaldj.cb8uwtsbobah.us-west-2.rds.amazonaws.com,1433;Initial Catalog=DJJServer;Persist Security Info=True;User Id=DJJServer;Password=DigitalDJ!;";

        public SessionData GetSession(string sessionId)
        {
            var myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            var myCommand = new SqlCommand("select * FROM [DJJServer].[dbo].[tblSessions] where session_id='" + sessionId + "'", myConnection);
            var myReader = myCommand.ExecuteReader();
            while (myReader.Read())
            {
                var id = myReader["session_id"].ToString();
                var users = myReader["users"].ToString();
                var recentSongs = myReader["recent_songs"].ToString();
                var lastUpdate = (DateTime)myReader["last_update"];
                myConnection.Close();
                return new SessionData(id, users, recentSongs, lastUpdate);
            }
            return null;
        }

        public void CreateSession(SessionData session)
        {
            if (!IsSessionExistant(session.session_id))
            {
                var query = "INSERT INTO [dbo].[tblSessions] ([session_id], [users], [recent_songs], [last_update]) VALUES ('" + session.session_id + "','" + session.users + "','" + session.recent_songs + "','" + session.last_update + "')";
                var myConnection = new SqlConnection(connectionString);
                myConnection.Open();
                var myCommand = new SqlCommand(query, myConnection);
                var myReader = myCommand.ExecuteReader();
                myConnection.Close();
            }
            else
            {
                throw new Exception("Session already exists for session_id=" + session.session_id);
            }
        }

        public bool DeleteSession(string sessionId)
        {
            if (IsSessionExistant(sessionId))
            {
                var query = "DELETE FROM [dbo].[tblSessions] WHERE [session_id] = '" + sessionId + "'";
                var myConnection = new SqlConnection(connectionString);
                myConnection.Open();
                var myCommand = new SqlCommand(query, myConnection);
                var myReader = myCommand.ExecuteReader();
                myConnection.Close();
                return true; //TODO: Verify this is actually true
            }
            else
            {
                return false;
            }
        }

        public void UpdateSession(SessionData session)
        {
            if (IsSessionExistant(session.session_id))
            {
                var query = "UPDATE [dbo].[tblSessions] SET [users] = '"+session.users+ "' ,[recent_songs] = '"+session.recent_songs+ "', [last_update] = '"+session.last_update+"' WHERE [session_id] ='" + session.session_id + "'";
                var myConnection = new SqlConnection(connectionString);
                myConnection.Open();
                var myCommand = new SqlCommand(query, myConnection);
                var myReader = myCommand.ExecuteReader();
                myConnection.Close();   
            }
            else
            {
                throw new Exception("Session does not exist, nothing to update for session_id=" + session.session_id);
            }
        }

        public bool IsSessionExistant(string sessionId)
        {
            var myConnection = new SqlConnection(connectionString);
            myConnection.Open();
            var myCommand = new SqlCommand("select * FROM [DJJServer].[dbo].[tblSessions] where session_id='" + sessionId + "'", myConnection);
            var myReader = myCommand.ExecuteReader();
            var isSessionExistant = myReader.Read();
            myConnection.Close();

            return isSessionExistant;
        }
    }
}
