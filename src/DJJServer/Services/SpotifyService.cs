﻿using DJJServer.Models.Spotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DJJServer.Services
{
    public interface ISpotifyService
    {
        Track GetTrackById(string songId);
        Artist GetArtistById(string artistId);
    }

    public class SpotifyService : ISpotifyService
    {
        public Track GetTrackById(string songId)
        {
            var url = "https://api.spotify.com/v1/tracks/" + songId;
            return Helpers.HttpClientHelper.Get<Track>(url);
        }

        public Artist GetArtistById(string artistId)
        {
            var url = "https://api.spotify.com/v1/artists/" + artistId;
            return Helpers.HttpClientHelper.Get<Artist>(url);
        }
    }
}