﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DJJServer.Data;
using DJJServer.Models.Api;
using DJJServer.DataAccess;
using DJJServer.Models.SessionData;
using Newtonsoft.Json;

namespace DJJServer.Services
{
    public interface ISessionService
    {
        string NewSession();
        bool EndSession(string sessionId);
        string NewUser(string sessionId, List<string> songIds);
        bool RemoveUser(string sessionId, string userId);
        bool UpdateUser(string sessionId, string userId, List<string> songIds);
        void AddToRecentSongs(string sessionId, string songId);
        void RemoveFromEndOfRecentSongs(string sessionId);
        Session GetSession(string sessionId);
    }
    public class SessionService : ISessionService
    {
        private SessionDataAccess SessionDataAcess { get; set; } = new SessionDataAccess();

        public string NewSession()
        {
            string sessionId = Guid.NewGuid().ToString();

            var createdSession = new Session
            {
                Id = sessionId,
                Users = new List<User>(),
                RecentSongs = new Queue<string>(5),
                LastUpdate = DateTime.Now
            };

            SessionDataAcess.CreateSession(ConvertToSessionData(createdSession));

            return sessionId;
        }

        public bool EndSession(string sessionId)
        {
            var exists = SessionDataAcess.IsSessionExistant(sessionId);
            exists = exists && SessionDataAcess.DeleteSession(sessionId);
            return exists;
        }

        public string NewUser(string sessionId, List<string> songIds)
        {
            string userId = Guid.NewGuid().ToString();
            Session session = GetSession(sessionId);
            if (session != null)
            {
                if (session.Users == null)
                {
                    session.Users = new List<User>();
                }
                session.Users.Add(new User(userId, songIds));
                SessionDataAcess.UpdateSession(ConvertToSessionData(session));
                return userId;
            }
            throw new Exception("Unable to add user with OAuth Token: " + songIds);
        }

        public bool RemoveUser(string sessionId, string userId)
        {
            Session session = GetSession(sessionId);
            if (session != null)
            {
                var userToRemove = session.Users.FirstOrDefault(aUser => aUser.UserId.Equals(userId));
                session.Users.Remove(userToRemove);
                SessionDataAcess.UpdateSession(ConvertToSessionData(session));
                return userToRemove != null;
            }
            return false;
        }

        public bool UpdateUser(string sessionId, string userId, List<string> songIds)
        {
            Session session = GetSession(sessionId);
            var userToUpdate = session.Users.FirstOrDefault(aUser => aUser.UserId.Equals(userId));
            var userExists = userToUpdate != null;
            
            if (userExists)
            {
                userToUpdate.SongIds = songIds;
                SessionDataAcess.UpdateSession(ConvertToSessionData(session));
            }

            return userExists;
        }

        public Session GetSession(string sessionId)
        { 
            var session = ConvertToSession(SessionDataAcess.GetSession(sessionId));
            
            return session;
        }

        public User GetUser(string sessionId, string userId)
        {
            if (!SessionDataAcess.IsSessionExistant(sessionId))
            {
                return null;
            }
            if (GetSession(sessionId).Users == null)
            {
                return null;
            }
            return GetSession(sessionId).Users.Where(aUser => aUser.UserId == userId).ToList().First();
        }

        public void AddToRecentSongs(string sessionId, string songId)
        {
            Session session = GetSession(sessionId);

            if (session != null && session.RecentSongs != null)
            {
                var recentSongs = session.RecentSongs;
                while (recentSongs.Count >= 20)
                {
                    recentSongs.Dequeue();
                }
                recentSongs.Enqueue(songId);
                SessionDataAcess.UpdateSession(ConvertToSessionData(session));
            }
        }

        public void RemoveFromEndOfRecentSongs(string sessionId)
        {
            Session session = GetSession(sessionId);

            if (session != null && session.RecentSongs != null)
            {
                var recentSongs = session.RecentSongs;
                var first = recentSongs.Peek();
                string current = null;
                while (true)
                {
                    current = recentSongs.Dequeue();
                    if (recentSongs.Peek() == first)
                    {
                        break;
                    }
                    recentSongs.Enqueue(current);
                }
                SessionDataAcess.UpdateSession(ConvertToSessionData(session));
            }
        }

        public Session ConvertToSession(SessionData sessionData)
        {
            if (sessionData == null)
            {
                return null;
            }
            var convertedSession = new Session();
            var convertedUsers = JsonConvert.DeserializeObject<List<User>>(sessionData.users);
            var convertedRecentSongs = JsonConvert.DeserializeObject<Queue<string>>(sessionData.recent_songs);

            return new Session()
            {
                Id = sessionData.session_id,
                Users = convertedUsers,
                RecentSongs = convertedRecentSongs,
                LastUpdate = sessionData.last_update
            }; 
        }

        public SessionData ConvertToSessionData(Session session)
        {
            if (session == null)
            {
                return null;
            }
            session.Touch();
            var convertedUsers = JsonConvert.SerializeObject(session.Users);
            var convertedRecentSongs = JsonConvert.SerializeObject(session.RecentSongs);

            return new SessionData(session.Id, convertedUsers, convertedRecentSongs, session.LastUpdate);
        }

    }
}
