﻿using System;
using System.Collections.Generic;
using System.Linq;
using DJJServer.Data;
using DJJServer.DataAccess;
using DJJServer.Models;
using DJJServer.Models.Api;
using DJJServer.Models.Spotify;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DJJServer.Services
{
    public class PlaylistGenerator
    {
        private readonly ISessionService _sessionService;

        public PlaylistGenerator(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        public List<string> GeneratePlaylist(string sessionId, int playlistLength, int songsToRecycle = 0)
        {
            var session = _sessionService.GetSession(sessionId);
            while (songsToRecycle > 0)
            {
                _sessionService.RemoveFromEndOfRecentSongs(session.Id);
            }
            var generatedPlaylist = new HashSet<string> { };

            generatedPlaylist = FillWithCrowdFavorites(session, generatedPlaylist, playlistLength);

            return FillWithTrendingMusic(session, generatedPlaylist.ToList(), playlistLength);
        }

        private readonly double _threshHold = 0.5;

        public HashSet<string> FindCrowdFavorites(Session session, HashSet<string> playlist, int length = -1)
        {
            var favorites = session.Users.SelectMany(user => user.SongIds).GroupBy(s => s).Where(group => group.Count() / session.Users.Count > _threshHold).SelectMany(grp => grp).Distinct().ToList();
            var random = new Random();

            while ( favorites.Count > 0 && playlist.Count < length )
            {
                var i = random.Next(0, favorites.Count);
                if (!session.RecentSongs.Contains(favorites[i]))
                {
                    playlist.Add(favorites[i]);
                    _sessionService.AddToRecentSongs(session.Id, favorites[i]);
                }
                favorites.RemoveAt(i);
            }

            return playlist;
        }

        private Song GetSong(string trackId)
        {
            var spotifyService = new SpotifyService();
            var artists = spotifyService.GetTrackById(trackId).Artists.Select( a => a.Id).ToList();
            var album = spotifyService.GetTrackById(trackId).Album.Id;
            var genres = new HashSet<string>();
            for (var i = 0; i < artists.Count; i++)
            {
                var artist = spotifyService.GetArtistById(artists[i]);
                foreach (var genre in artist.Genres)
                {
                    genres.Add(genre);
                }
            }
            return new Song(trackId, album, artists, genres.ToList(), 0);
        }

        public HashSet<string> FillWithCrowdFavorites(Session session, HashSet<string> playlist, int length = -1)
        {
            var allSongs = session.Users.SelectMany(user => user.SongIds).ToList();
            var distinctSongs = session.Users.SelectMany(user => user.SongIds).Distinct().ToList();
            var weightedSongs = new Dictionary<string, Song>();

            for (var i = 0; i < distinctSongs.Count; i++)
            {
                weightedSongs[distinctSongs[i]] = GetSong(distinctSongs[i]);
            }

            foreach (var trackId in new List<string>(weightedSongs.Keys))
            {
                var song = weightedSongs[trackId];
                for (var i = 0; i < allSongs.Count; i++)
                {
                    var comparisonSong = weightedSongs[allSongs[i]];
                    if (song.SongId == comparisonSong.SongId)
                    {
                        song.Weight += 4;
                    }
                    else if (song.Album == comparisonSong.Album)
                    {
                        song.Weight += 3;
                    }
                    else if (song.Artists.Any(comparisonSong.Artists.Contains))
                    {
                        song.Weight += 2;
                    }
                    else if (song.Genres.Any(comparisonSong.Genres.Contains))
                    {
                        song.Weight++;
                    }
                }
                weightedSongs[trackId] = song;
            }

            var orderedSongs = weightedSongs.Values.OrderByDescending(s => s.Weight).Select(s => s.SongId).ToList();

            while (orderedSongs.Count > 0 && playlist.Count < length)
            {
                if (!session.RecentSongs.Contains(orderedSongs.First()))
                {
                    playlist.Add(orderedSongs.First());
                    _sessionService.AddToRecentSongs(session.Id, orderedSongs.First());
                }
                orderedSongs.RemoveAt(0);
            }

            return playlist;
        }

        public List<string> FillWithTrendingMusic(Session session, List<string> playlist, int length = -1)
        {
            var defaultSongToken = "7GhIk7Il098yCjg4BQjzvb";

            while (playlist.Count < length)
            {
                playlist.Add(defaultSongToken);
                _sessionService.AddToRecentSongs(session.Id, defaultSongToken);
                // TODO: Get trending / popular music from spotify
            }

            return playlist;
        }
    }
}
