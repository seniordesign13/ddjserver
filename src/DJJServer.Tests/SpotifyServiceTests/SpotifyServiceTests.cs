﻿using DJJServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DJJServer.Tests.SpotifyServiceTests
{
    public class SpotifyServiceTests
    {
        [Fact]
        public void SpotifyServiceTest()
        {
            var track = new SpotifyService().GetTrackById("7GhIk7Il098yCjg4BQjzvb");
            Assert.Equal("Never Gonna Give You Up".ToLowerInvariant(),  track.Name.ToLowerInvariant());

            var artist = new SpotifyService().GetArtistById(track.Artists.FirstOrDefault().Id);
            Assert.Equal("Rick Astley".ToLowerInvariant(), artist.Name.ToLowerInvariant());

        }
    }
}
