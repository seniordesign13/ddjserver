﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DJJServer.Models.Spotify;

namespace DJJServer.Tests
{
    public class TestData
    {

        public static string GetTrackJson()
        {
            return @"{
  'album' : {
    'album_type' : 'album',
    'artists' : [ {
      'external_urls' : {
        'spotify' : 'https://open.spotify.com/artist/1TJbmc7jTpw78GKCiMpvDh'
      },
      'href' : 'https://api.spotify.com/v1/artists/1TJbmc7jTpw78GKCiMpvDh',
      'id' : '1TJbmc7jTpw78GKCiMpvDh',
      'name' : 'Blue October',
      'type' : 'artist',
      'uri' : 'spotify:artist:1TJbmc7jTpw78GKCiMpvDh'
    } ],
    'available_markets' : [ 'AD', 'AR', 'AT', 'AU', 'BE', 'BG', 'BO', 'BR', 'CA', 'CH', 'CL', 'CO', 'CR', 'CY', 'CZ', 'DE', 'DK', 'DO', 'EC', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'GT', 'HK', 'HN', 'HU', 'ID', 'IE', 'IS', 'IT', 'JP', 'LI', 'LT', 'LU', 'LV', 'MC', 'MT', 'MX', 'MY', 'NI', 'NL', 'NO', 'NZ', 'PA', 'PE', 'PH', 'PL', 'PT', 'PY', 'SE', 'SG', 'SK', 'SV', 'TR', 'TW', 'US', 'UY' ],
    'external_urls' : {
      'spotify' : 'https://open.spotify.com/album/1ieixdUcYhgtn8naIc8Ez8'
    },
    'href' : 'https://api.spotify.com/v1/albums/1ieixdUcYhgtn8naIc8Ez8',
    'id' : '1ieixdUcYhgtn8naIc8Ez8',
    'images' : [ {
      'height' : 640,
      'url' : 'https://i.scdn.co/image/816412fe2657d646fec76300f33f7f15996d04bf',
      'width' : 640
    }, {
      'height' : 300,
      'url' : 'https://i.scdn.co/image/c5a17ec7b5d2095e952afee3e7064dd1ffbb5589',
      'width' : 300
    }, {
      'height' : 64,
      'url' : 'https://i.scdn.co/image/012a9ae3ac6ac431ff15c3761ae07ba09abf4ad9',
      'width' : 64
    } ],
    'name' : 'Sway',
    'type' : 'album',
    'uri' : 'spotify:album:1ieixdUcYhgtn8naIc8Ez8'
  },
  'artists' : [ {
    'external_urls' : {
      'spotify' : 'https://open.spotify.com/artist/1TJbmc7jTpw78GKCiMpvDh'
    },
    'href' : 'https://api.spotify.com/v1/artists/1TJbmc7jTpw78GKCiMpvDh',
    'id' : '1TJbmc7jTpw78GKCiMpvDh',
    'name' : 'Blue October',
    'type' : 'artist',
    'uri' : 'spotify:artist:1TJbmc7jTpw78GKCiMpvDh'
  } ],
  'available_markets' : [ 'AD', 'AR', 'AT', 'AU', 'BE', 'BG', 'BO', 'BR', 'CA', 'CH', 'CL', 'CO', 'CR', 'CY', 'CZ', 'DE', 'DK', 'DO', 'EC', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'GT', 'HK', 'HN', 'HU', 'ID', 'IE', 'IS', 'IT', 'JP', 'LI', 'LT', 'LU', 'LV', 'MC', 'MT', 'MX', 'MY', 'NI', 'NL', 'NO', 'NZ', 'PA', 'PE', 'PH', 'PL', 'PT', 'PY', 'SE', 'SG', 'SK', 'SV', 'TR', 'TW', 'US', 'UY' ],
  'disc_number' : 1,
  'duration_ms' : 296000,
  'explicit' : true,
  'external_ids' : {
    'isrc' : 'USZCN1300026'
  },
  'external_urls' : {
    'spotify' : 'https://open.spotify.com/track/3dqOijXsUF9EOg8vh891ae'
  },
  'href' : 'https://api.spotify.com/v1/tracks/3dqOijXsUF9EOg8vh891ae',
  'id' : '3dqOijXsUF9EOg8vh891ae',
  'name' : 'Things We Do at Night',
  'popularity' : 34,
  'preview_url' : 'https://p.scdn.co/mp3-preview/f5a1931ddd450ba73a9159e7855953bdc1923480?cid=8897482848704f2a8f8d7c79726a70d4',
  'track_number' : 11,
  'type' : 'track',
  'uri' : 'spotify:track:3dqOijXsUF9EOg8vh891ae'
}";
        }

        public static List<PagingObject<Track>> GetListOfTracks()
        {
            var listOfTracks = new List<PagingObject<Track>>();

            var trackItems1 = new List<Track>();
            var trackItems2 = new List<Track>();
            var trackItems3 = new List<Track>();

            trackItems1.Add(new Track() {Id = "id1"});
            trackItems1.Add(new Track() { Id = "id2" });
            trackItems1.Add(new Track() { Id = "id3" });
            trackItems1.Add(new Track() { Id = "id23" });
            trackItems1.Add(new Track() { Id = "id503" });


            trackItems2.Add(new Track() { Id = "id1" });
            trackItems2.Add(new Track() { Id = "id2" });
            trackItems2.Add(new Track() { Id = "id8" });
            trackItems2.Add(new Track() { Id = "id80" });
            trackItems2.Add(new Track() { Id = "id503" });


            trackItems3.Add(new Track() { Id = "id1" });
            trackItems3.Add(new Track() { Id = "id2" });
            trackItems3.Add(new Track() { Id = "id10" });
            trackItems3.Add(new Track() { Id = "id43" });
            trackItems3.Add(new Track() { Id = "id503" });


            var pagingObject1 = new PagingObject<Track> {Items = trackItems1};
            var pagingObject2 = new PagingObject<Track> { Items = trackItems2 };
            var pagingObject3 = new PagingObject<Track> { Items = trackItems3 };

            listOfTracks.Add(pagingObject1);
            listOfTracks.Add(pagingObject2);
            listOfTracks.Add(pagingObject3);

            return listOfTracks;
        }
    }
}
