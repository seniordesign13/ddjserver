﻿using System.Linq;
using DJJServer.Models.Spotify;
using Newtonsoft.Json;
using Xunit;

namespace DJJServer.Tests.JsonToModelTests
{ 
    public class JsonToModelsTests
    {
        [Fact]
        public void GivenJsonRepresentsATrack_WhenMappingToObject_ThenObjectShouldBeValidTrackObject()
        {
            Track track = JsonConvert.DeserializeObject<Track>(TestData.GetTrackJson());

            var trackNameActual = track.Name;
            var artistNameActual = track.Artists.FirstOrDefault().Name;

            Assert.Equal("Things We Do at Night", trackNameActual);
            Assert.Equal("Blue October", artistNameActual); 
        }
    }
}
