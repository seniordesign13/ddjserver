﻿using System.Collections.Generic;
using System.Linq;
using DJJServer.Services;
using Xunit;
using DJJServer.DataAccess;

namespace DJJServer.Tests.SessionServiceTests
{
    public class SessionServiceTests
    {
        [Fact]
        public void GivenASessionNeedsToBeCreated_WhenASessionIsCreated_ThenTheSessionIdExists()
        {
            var sessionService = new SessionService();
            var sessionId = sessionService.NewSession();
            Assert.True(new SessionDataAccess().IsSessionExistant(sessionId));
        }

        [Fact]
        public void GivenAUserNeedsToBeAdded_WhenAUserIsAdded_ThenTheUserExists()
        {
            var sessionService = new SessionService();
            var sessionId = sessionService.NewSession();
            var song1 = "song1";
            var song2 = "song2";
            var song3 = "song3";

            var userId = sessionService.NewUser(sessionId, new List<string>() {song1, song2, song3});

            Assert.True(sessionService.GetSession(sessionId).Users.Select(aUser => aUser.UserId).ToList().Contains(userId));
            Assert.Equal(userId, sessionService.GetUser(sessionId, userId).UserId);
            var user = sessionService.GetUser(sessionId, userId);
            Assert.Contains(song1, user.SongIds);
            Assert.Contains(song2, user.SongIds);
            Assert.Contains(song3, user.SongIds);
        }

        [Fact]
        public void GivenMultipleSessionsAndUsersExist_WhenASpecificSessonOrUserIsRequested_ThenItIsAvailable()
        {
            var sessionService = new SessionService();
            var sessionId1 = sessionService.NewSession();
            var sessionId1User1 = sessionService.NewUser(sessionId1, new List<string>() {"song1", "song2", "blank", "songz" });
            var sessionId2 = sessionService.NewSession();
            sessionService.NewUser(sessionId2, new List<string>() { "song3", "song4", "song5", "blank", "songz" });
            sessionService.NewUser(sessionId2, null);
            var sessionId3 = sessionService.NewSession();
            var sessionId3User1 = sessionService.NewUser(sessionId3, new List<string>() { "song6", "song7", "song8", "song9", "songz" });
            sessionService.NewUser(sessionId3, null);
            sessionService.NewUser(sessionId3, null);

           // Assert.Equal(3, sessionService.Sessions.Keys.Count);

            Assert.Equal(1, sessionService.GetSession(sessionId1).Users.Count);
            Assert.Equal(2, sessionService.GetSession(sessionId2).Users.Count);
            Assert.Equal(3, sessionService.GetSession(sessionId3).Users.Count);

            Assert.Equal(4, sessionService.GetUser(sessionId1, sessionId1User1).SongIds.Count);
            Assert.True(sessionService.GetUser(sessionId1, sessionId1User1).SongIds.Contains("song1"));

            Assert.Equal(5, sessionService.GetUser(sessionId3, sessionId3User1).SongIds.Count);
            Assert.True(sessionService.GetUser(sessionId3, sessionId3User1).SongIds.Contains("song6"));
        }

        [Fact]
        public void GivenMultipleSessionsAndUsersExist_WhenASessionOrUserIsRemoved_ThenTheyWillBeGone()
        {
            var sessionService = new SessionService();
            var sessionId1 = sessionService.NewSession();
            var sessionId1User1 = sessionService.NewUser(sessionId1, new List<string>() {"song1", "song2", "blank", "songz" });
            var sessionId2 = sessionService.NewSession();
            sessionService.NewUser(sessionId2, new List<string>() { "song3", "song4", "song5", "blank", "songz" });
            sessionService.NewUser(sessionId2, null);
            var sessionId3 = sessionService.NewSession();
            var sessionId3User1 = sessionService.NewUser(sessionId3, new List<string>() { "song6", "song7", "song8", "song9", "songz" });
            sessionService.NewUser(sessionId3, null);
            sessionService.NewUser(sessionId3, null);

            sessionService.EndSession(sessionId2);
            Assert.Equal(null, sessionService.GetSession(sessionId2));

            sessionService.RemoveUser(sessionId1, sessionId1User1);
            Assert.Equal(0, sessionService.GetSession(sessionId1).Users.Count);
            Assert.Equal(null, sessionService.GetSession(sessionId2));
            Assert.Equal(3, sessionService.GetSession(sessionId3).Users.Count);
        }

        [Fact]
        public void GivenAUserNeedsToBeUpdated_WhenTheUserIsUpdated_ThenTheChangesAreVisible()
        {
            var sessionService = new SessionService();
            var sessionId1 = sessionService.NewSession();
            var sessionId1User1 = sessionService.NewUser(sessionId1, new List<string>() { "song1", "song2", "blank", "songz" });

            Assert.Equal(4, sessionService.GetUser(sessionId1, sessionId1User1).SongIds.Count);

            sessionService.UpdateUser(sessionId1, sessionId1User1, new List<string>() {"song1", "song2"});

            Assert.Equal(2, sessionService.GetUser(sessionId1, sessionId1User1).SongIds.Count);
        }
    }
}
