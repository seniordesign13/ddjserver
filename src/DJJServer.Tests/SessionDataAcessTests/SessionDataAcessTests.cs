﻿using DJJServer.DataAccess;
using DJJServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DJJServer.Tests.SessionDataAcessTests
{
    public class SessionDataAccessTests
    {
        [Fact]
        public void GetSessionTest()
        {
            var dataAcess = new SessionDataAccess();
            dataAcess.GetSession("0");
        }

        [Fact]
        public void ExistingSessionTest()
        {
            var sessionDataAcess = new SessionDataAccess();
            var sessionService = new Services.SessionService();
            var sessionId = sessionService.NewSession();
            Assert.True(sessionDataAcess.IsSessionExistant(sessionId));
            sessionService.EndSession(sessionId);
            Assert.False(sessionDataAcess.IsSessionExistant(sessionId));
        }

        [Fact]
        public void CreateSessionDeleteSessionTest()
        {
            var dataAccess = new SessionDataAccess();
            var sessionId = "9999";
            Assert.False(dataAccess.IsSessionExistant(sessionId));
            dataAccess.CreateSession(new Models.SessionData.SessionData(sessionId, "list_of_users", "list_of_recent_songs", DateTime.Now));
            Assert.True(dataAccess.IsSessionExistant(sessionId));
            dataAccess.DeleteSession(sessionId);
            Assert.False(dataAccess.IsSessionExistant(sessionId));
        }

        [Fact]
        public void UpdateSessionTest()
        {
            var dataAccess = new SessionDataAccess();
            var sessionId = "9999";
            dataAccess.CreateSession(new Models.SessionData.SessionData(sessionId, "list_of_users", "list_of_recent_songs", DateTime.Now));
            var dataAcess = new SessionDataAccess();
            var expectedUsers = Guid.NewGuid().ToString();
            var expectedRecentSongs = Guid.NewGuid().ToString();
            var expectedLastUpdate = new DateTime(new Random().Next(1900, 2040), new Random().Next(1, 13), new Random().Next(1, 32));
            var sessionData = new Models.SessionData.SessionData(sessionId, expectedUsers, expectedRecentSongs, expectedLastUpdate);

            dataAcess.UpdateSession(sessionData);

            var actual = dataAcess.GetSession(sessionId);
            Assert.Equal(expectedUsers, actual.users);
            Assert.Equal(expectedRecentSongs, actual.recent_songs);
            Assert.Equal(expectedLastUpdate, actual.last_update);
            dataAccess.DeleteSession(sessionId);

        }
    }
}
