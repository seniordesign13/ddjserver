﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DJJServer.Models;
using DJJServer.Services;
using Newtonsoft.Json;
using Xunit;
using Moq;

namespace DJJServer.Tests.PlaylistGenerationTests
{
    public class CreatePlaylistByTopTracksTest
    {

        /*
         * 
        Animal I have become: album x [ "alternative metal", "canadian metal", "nu metal", "pop punk", "post-grunge" ]
        56sk7jBpZV0CD31G9hEU3b

        Never too late:  album x
        3HE50TVRquwXe9yv2HFoNL

        I hate everything about you: album y 
        6rUp7v3l8yC4TKxAAR5Bmx

        Inside the fire: album z [ "alternative metal", "nu metal", "post-grunge", "rap metal" ]
        5cxp9kjCFyJwzv3lzeX7ku

        Remember when: album b [ "contemporary country", "country", "country christmas", "country road" ]
        1nCSr7oGeUv7sUEoYuAJFF     
        
        */

        [Fact]
        public void GivenAListOfSongsWithSimilarNamesArtistsAlbumsAndGenres_WhenAPlaylistIsGenerated_ThenThePlaylistShouldBeWeighted_1Songs()
        {
            SessionService sessionService = new SessionService();
            PlaylistGenerator playlistGen = new PlaylistGenerator(sessionService);
            var sessionId = sessionService.NewSession();
            var user1 = sessionService.NewUser(sessionId, new List<string> { "56sk7jBpZV0CD31G9hEU3b" });
            var user2 = sessionService.NewUser(sessionId, new List<string> { "56sk7jBpZV0CD31G9hEU3b" });
            var user3 = sessionService.NewUser(sessionId, new List<string> { "3HE50TVRquwXe9yv2HFoNL" });
            var user4 = sessionService.NewUser(sessionId, new List<string> { "6rUp7v3l8yC4TKxAAR5Bmx" });
            var user5 = sessionService.NewUser(sessionId, new List<string> { "5cxp9kjCFyJwzv3lzeX7ku" });
            var user6 = sessionService.NewUser(sessionId, new List<string> { "1nCSr7oGeUv7sUEoYuAJFF" });

            var generatedPlaylist = playlistGen.GeneratePlaylist(sessionId, 7);

            Assert.Equal("56sk7jBpZV0CD31G9hEU3b", generatedPlaylist[0]);
            Assert.Equal("3HE50TVRquwXe9yv2HFoNL", generatedPlaylist[1]);
            Assert.Equal("6rUp7v3l8yC4TKxAAR5Bmx", generatedPlaylist[2]);
            Assert.Equal("5cxp9kjCFyJwzv3lzeX7ku", generatedPlaylist[3]);
            Assert.Equal("1nCSr7oGeUv7sUEoYuAJFF", generatedPlaylist[4]);
        }

        [Fact]
        public void GivenAListOfSongsWithSimilarNamesArtistsAlbumsAndGenres_WhenAPlaylistIsGenerated_ThenThePlaylistShouldBeWeighted_2Songs()
        {
            SessionService sessionService = new SessionService();
            PlaylistGenerator playlistGen = new PlaylistGenerator(sessionService);
            var sessionId = sessionService.NewSession();
            var user1 = sessionService.NewUser(sessionId, new List<string> { "56sk7jBpZV0CD31G9hEU3b" });
            var user2 = sessionService.NewUser(sessionId, new List<string> { "56sk7jBpZV0CD31G9hEU3b" });
            var user3 = sessionService.NewUser(sessionId, new List<string> { "3HE50TVRquwXe9yv2HFoNL" });
            var user4 = sessionService.NewUser(sessionId, new List<string> { "6rUp7v3l8yC4TKxAAR5Bmx" });
            var user5 = sessionService.NewUser(sessionId, new List<string> { "5cxp9kjCFyJwzv3lzeX7ku" });
            var user6 = sessionService.NewUser(sessionId, new List<string> { "1nCSr7oGeUv7sUEoYuAJFF" });

            var generatedPlaylist = playlistGen.GeneratePlaylist(sessionId, 2);

            Assert.Equal("56sk7jBpZV0CD31G9hEU3b", generatedPlaylist[0]);
            Assert.Equal("3HE50TVRquwXe9yv2HFoNL", generatedPlaylist[1]);
        }
    }
}
